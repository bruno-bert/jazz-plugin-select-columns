export const name = "jazz-plugin-select-columns";
export const pipeline = [
  {
    id: "selectColumns",
    description: "Select a set of columns and rename the header names",
    class: "SelectColumnsTask"
  }
];

/*
export const inputParameters = {
  inputParm1: {
    attr1: "attr 1 - value 1",
    attr2: "attr 2 - value 2",    
  }
};*/
