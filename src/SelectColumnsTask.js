/* eslint-disable class-methods-use-this */
import Task from "./Task";
import { name as pluginName } from "./config";
import { selectColumns, renameColumns } from "./helpers";

class SelectColumnsTask extends Task {
  constructor(id, params, config, description = "", rawDataFrom = null) {
    super(id, params, config, description, rawDataFrom);

    [this.plugin] = this.config.plugins.filter(
      plugin => plugin.name === pluginName
    );

    this.validateParameters({
      ...params,
      columns: this.plugin.tasks[id].columns
    });
  }

  execute(data) {
    this.rawData = data || this.getRawData();
    const columns = this.plugin.tasks[this.id].columns;

    if (!this.validateConditionsForExecution()) return;

    return new Promise((resolve, reject) => {
      const result = selectColumns(this.rawData, columns);
      resolve({ renameMapping: columns, result });
    });
  }

  onSuccess(info) {
    super.onSuccess(info);

    this.logger.success(
      `${String(this.constructor.name)} - Columns selected successfully`
    );
  }

  validateParameters(params) {
    if (!params.columns) {
      this.onError("Set of Columns to be selected is required");
    }
  }
}
module.exports = SelectColumnsTask;
