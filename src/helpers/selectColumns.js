const pick = require("lodash.pick");
const selectColumns = (data, columns) => {
  const columnIds = columns.map(column => column.id);
  const subset = data.map(item => {
    const subItem = pick(item, columnIds);
    return subItem;
  });

  const removeEmptiesFromSubSet = subset.filter(
    item => Object.entries(item).length !== 0
  );

  return removeEmptiesFromSubSet;
};

module.exports = selectColumns;
