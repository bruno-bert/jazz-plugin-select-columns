const source = "this";
const defaultRelativePluginPath = "../..";
const excelExtractorPluginPath = `${defaultRelativePluginPath}/jazz-plugin-excelextractor/dist`;
const FromPipeline = require("../FromPipeline");

module.exports = {
  pipeline: [
    `${excelExtractorPluginPath}:jazz-plugin-excelextractor:extract`,
    `${excelExtractorPluginPath}:jazz-plugin-excelextractor:validate`,
    `${source}:jazz-plugin-select-columns:selectColumns`
  ],
  plugins: [
    {
      name: "jazz-plugin-excelextractor",
      sheet: "source"
    },
    {
      name: "jazz-plugin-select-columns",
      tasks: {
        selectColumns: {
          postExecute: () => {
            /* this is only to test the view of data with the renamed columns 
            the plugin "select-columns" will only be responsible for attaching the view configuration into the pipeline to the next plugin 
            the plugins that will render data on screen will need to take care of using this view config to render the data with in correct way
            in the case of the renaming, for example, the render plugin will need to use the view config to render the data with new column names
            */
            const data = FromPipeline.getResult("selectColumns");
            const jsonData = JSON.stringify(data.result);
            console.log(data);
            console.log(
              jsonData
                .replace("employee_name", "Nome do Empregado")
                .replace("account", "Conta Bancária")
            );
          },

          rawDataFrom: "extract",
          columns: [
            {
              id: "employee_name",
              renameTo: "Nome do Empregado"
            },
            {
              id: "account",
              renameTo: "Conta Bancária"
            }
          ]
        }
      }
    }
  ]
};
