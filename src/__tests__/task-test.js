/* eslint-disable import/no-extraneous-dependencies */
import { createConfigInfo } from "jazz-core/dist/helpers";
import SelectColumnsTask from "../SelectColumnsTask";

/** this is used to unit test the task run, without using the pipeline
 *  in this case the methods ovewritten on pack-config.js file will not get captured
 *  for example, it will not work if you ovewrite the getRawData method on pack-config
 *  so, for testing purposes, you need to set the raw data before calling the execute method
 */

const configFile = "./src/__tests__/pack-config";
const config = createConfigInfo(configFile);
const task = new SelectColumnsTask("selectColumns", {}, config);

const data = require("./data.json");
task.setRawData(data);
task.execute(data);
