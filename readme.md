# Jazz Plugin: Select Columns Documentation

**If you want to be a contributor, [write to us](mailto:jazzcoreteam@gmail.com)**

Also, access the [Jazz Documentation website](https://jazz-docs.netlify.com) for more information.

## Plugin Pipeline

### Tasks

Contains a single task, named _selectColumns_.

#### selectColumns

**Purpose**: _selectColumns_ task is responsable for selecting the received data and extract only a specific set of columns.
It will also add the view configuration with renamed columns into the pipeline.

**Class**: SelectColumnsTask

**Parameters**:

- columns: an array of columns in the format below. It contains the column id and it's corresponding name on view

```javascript
columns: [
  {
    id: "column_name_1",
    renameTo: "Any Column Name On View Mode 1"
  },
  {
    id: "column_name_2",
    renameTo: "Any Column Name On View Mode "
  }
];
```

> Note: This plugin will be responsible only to add the view configuration with **column id** and **view name** into the pipeline.
> This means that the rendering of this data with the correct names will be responsability of other plugins in the pipeline that will use this view configuration to handle the view. **Important:** The **column ids** need to correspond to the column ids in the raw data.

Example:

With this data input:

```javascript
"data": [
    {
      "amount": 2000,
      "customer_account_number": 12345,
      "account": 2233112399,
      "transaction_date": "010119",
      "bankname": "BANCO DE GUATEMALA",
      "employee_tax_id": "5435-464.56",
      "employee_id": "00000000000000000001",
      "employee_name": "John Rodriguez",
      "employee_address": "calle prueba 1",
      "update_type": 2
    },

    {
      "amount": 3000,
      "customer_account_number": 12345,
      "account": 2232999999,
      "transaction_date": "010119",
      "bankname": "BANCO DE GUATEMALA",
      "employee_tax_id": 1111,
      "employee_id": "00000000000000000002",
      "employee_name": "Maria José",
      "employee_address": "calle prueba 2",
      "update_type": 2
    }
]
```

And this configuration (in pack-config.js file):

```javascript
columns: [
  {
    id: "employee_name",
    renameTo: "Employee Name"
  },
  {
    id: "account",
    renameTo: "Bank Account"
  }
];
```

This will be the data output:

```javascript
{ renameMapping:
   [ { id: 'employee_name', renameTo: 'Nome do Empregado' },
     { id: 'account', renameTo: 'Conta Bancária' } ],
  result:
   [ { employee_name: 'John Rodriguez', account: 2233112399 },
     { employee_name: 'Maria José', account: 2232999999 }]
}
```

And then, another plugin responsible for rendering the data in the screen, will be able to get the output data and transform a view result into this:

```javascript
[
  {
    "Nome do Empregado": "John Rodriguez",
    "Conta Bancária": 2233112399
  },
  {
    "Nome do Empregado": "Maria José",
    "Conta Bancária": 2232999999
  }
];
```

### Pipeline

```javascript
export const pipeline = [
  {
    id: "selectColumns",
    description:
      "Select a set of columns and add the column names into the pipeline",
    class: "SelectColumns"
  }
];
```

### Input Parameters

_There are no input parameters to this plugin_
